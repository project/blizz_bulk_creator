<?php

namespace Drupal\blizz_bulk_creator\EntityStorage;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Class BulkcreateConfiguration.
 *
 * @package Drupal\blizz_bulk_creator\EntityStorage
 */
class BulkcreateConfiguration extends ConfigEntityStorage {
}
